<?php
/**
 * Created by PhpStorm.
 * User: Thomas Ven
 * Date: 11/05/2017
 * Time: 10:29
 */

namespace model;


/**
 * Class Event
 * @package model
 */
class Event implements \JsonSerializable
{
    /**
     * @var
     */
    private $id;
    /**
     * @var
     */
    private $pid;
    /**
     * @var
     */
    private $title;
    /**
     * @var
     */
    private $date;

    /**
     * Event constructor.
     * @param $id
     * @param $pid
     * @param $titel
     * @param $date
     */
    public function __construct($id, $pid, $title, $date)
    {
        $this->id = $id;
        $this->pid = $pid;
        $this->title = $title;
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param mixed $pid
     */
    public function setPid($pid)
    {
        $this->pid = $pid;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return[
            'id' => $this ->id,
            'pid' => $this -> pid,
            'titel' => $this -> title,
            'date' => $this -> date
        ];
    }
}