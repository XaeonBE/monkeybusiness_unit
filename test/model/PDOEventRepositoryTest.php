<?php

/**
 * Created by PhpStorm.
 * User: Thomas Ven
 * Date: 11/05/2017
 * Time: 10:26
 */

use \model\Event;
use \model\PDOEventRepository;

/**
 * Class PDOEventRepositoryTest
 */
class PDOEventRepositoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * Will create a mockEventRepository and a mockView.
     */
    public function setUp()
    {
        $this->mockPDO = $this->getMockBuilder('PDO')
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockPDOStatement = $this->getMockBuilder('PDOStatement')
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * Will destroy the mockEventRepository and the mockView
     */
    public function tearDown()
    {
        $this->mockPDO = null;
        $this->mockPDOStatement = null;
    }


    /**
     * Create new event
     * Check if it returns event
     * Make actual event and do getId
     * Check if the event and the actual event are the same
     */
    public function testFindEventById_idExists_EventObject()
    {
        $event = new Event(1, 1, 'testEvent', '2017-06-12 00:00:00');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchall')
            ->will($this->returnValue([['id' => $event->getId(),'persoon_id' => $event->getPid(),'titel' => $event->getTitle(), 'datum' => $event->getDate()]]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findByEventId($event->getId());
        $this->assertEquals($event, $actualEvent);
    }

    /**
     * checks if id can be obtained from database will fail if it can't be found
     */
    public function testFindEventById_idDoesNotExist_Null()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findByEventId(1);
        $this->assertNull($actualEvent);
    }

    /**
     * Checks if the exception is thrown tests fails if it got thrown
     */
    public function testFindEventById_exceptionThrownFromPDO_Null()
    {
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')->will($this->throwException(new Exception()));
        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findByEventId(1);
        $this->assertNull($actualEvent);
    }

    /**
     * Create new event
     * Check if it returns event
     * Make actual event and do getPid
     * Check if the event and the actual event are the same
     */
    public function testFindByPersonId_idExists_EventObject()
    {
        $event = new Event(1, 1, 'testEvent', '2017-06-12 00:00:00');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchall')
            ->will($this->returnValue([['id' => $event->getId(),'persoon_id' => $event->getPid(),'titel' => $event->getTitle(), 'datum' => $event->getDate()]]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findByPersonId($event->getPid());
        $this->assertEquals($event, $actualEvent);
    }

    /**
     * checks if personid can be obtained from database will fail if it can't be found
     */
    public function testFindPersonById_idDoesNotExist_Null()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findByPersonId(1);
        $this->assertNull($actualEvent);
    }

    /**
     * Checks if the exception is thrown tests fails if it got thrown
     */
    public function testFindPersonById_exceptionThrownFromPDO_Null()
    {
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')->will($this->throwException(new Exception()));
        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findByPersonId(1);
        $this->assertNull($actualEvent);
    }

    /**
     * Create new event
     * Check if it returns event
     * Make actual event and do getTitle
     * Check if the event and the actual event are the same
     */
    public function testFindEventByTitel_TitelExists_EventObject()
    {
        $event = new Event(1, 1, 'testEvent', '2017-06-12 00:00:00');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchall')
            ->will($this->returnValue([['id' => $event->getId(),'persoon_id' => $event->getPid(),'titel' => $event->getTitle(), 'datum' => $event->getDate()]]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findByTitle($event->getTitle());
        $this->assertEquals($event, $actualEvent);
    }

    /**
     * checks if titel can be obtained from database will fail if it can't be found
     */
    public function FindEventByTitel_titel_titelDoesNotExist_Null()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findByTitle('testEvent');
        $this->assertNull($actualEvent);
    }

    /**
     * Checks if the exception is thrown tests fails if it got thrown
     */
    public function FindEventByTitel_exceptionThrownFromPDO_Null()
    {
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')->will($this->throwException(new Exception()));
        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findByTitle('testEvent');
        $this->assertNull($actualEvent);
    }

    /**
     * Create new event
     * Check if it returns event
     * Make actual event and do getDate
     * Check if the event and the actual event are the same
     */
    public function testFindEventByDate_DateExists_EventObject()
    {
        $event = new Event(1, 1, 'testEvent', '2017-06-12 00:00:00');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchall')
            ->will($this->returnValue([['id' => $event->getId(),'persoon_id' => $event->getPid(),'titel' => $event->getTitle(), 'datum' => $event->getDate()]]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findByDate($event->getDate());
        $this->assertEquals($event, $actualEvent);
    }

    /**
     * checks if date can be obtained from database will fail if it can't be found
     */
    public function testFindEventByDate_dateDoesNotExist_Null()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findByDate('2017-06-12 00:00:00');
        $this->assertNull($actualEvent);
    }

    /**
     * Checks if the exception is thrown tests fails if it got thrown
     */
    public function testFindEventByDate_exceptionThrownFromPDO_Null()
    {
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')->will($this->throwException(new Exception()));
        $pdoRepository = new PDOEventRepository($this->mockPDO);
        $actualEvent = $pdoRepository->findByDate('2017-06-12 00:00:00');
        $this->assertNull($actualEvent);
    }

}