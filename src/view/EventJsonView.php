<?php
/**
 * Created by PhpStorm.
 * User: Thomas Ven
 * Date: 14/05/2017
 * Time: 15:49
 */

namespace view;


/**
 * Class EventJsonView
 * @package view
 */
class EventJsonView
{
    /**
     * Puts the data array out as a JSON string
     * @param array $data
     */
    public function show(array $data)
    {
        header('Content-Type: application/json');

        if (isset($data['events'])) {
            $events = $data['events'];
            echo json_encode($events);
        } else {
            echo '{}';
        }
    }
}