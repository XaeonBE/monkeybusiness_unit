<?php
/**
 * Created by PhpStorm.
 * User: Thomas Ven
 * Date: 11/05/2017
 * Time: 10:27
 */

require_once 'src/autoload.php';
use \model\PDOEventRepository;

$user = 'root';
$password = '';
$database = 'monkeybusiness_unit';
$pdo = null;

try {
  $pdo = new PDO("mysql:host=localhost;dbname=$database",
      $user, $password);
  $pdo->setAttribute(PDO::ATTR_ERRMODE,
      PDO::ERRMODE_EXCEPTION);

} catch (Exception $e) {
  echo 'cannot connect to database';
}


