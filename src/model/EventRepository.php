<?php
/**
 * Created by PhpStorm.
 * User: Thomas Ven
 * Date: 14/05/2017
 * Time: 15:53
 */

namespace model;

/**
 * Interface EventRepository
 * @package model
 */
interface EventRepository
{
    /**
     * @param $id
     * @return mixed
     */
    public function findByEventId($id);

    /**
     * @param $id
     * @return mixed
     */
    public function findByPersonId($id);

    /**
     * @param $title
     * @return mixed
     */
    public function findByTitle($title);

    /**
     * @param $date
     * @return mixed
     */
    public function findByDate($date);
}
