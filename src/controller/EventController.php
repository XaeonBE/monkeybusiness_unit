<?php
/**
 * Created by PhpStorm.
 * User: Thomas Ven
 * Date: 11/05/2017
 * Time: 10:46
 */

namespace controller;

use model\EventRepository;
use \view\View;

/**
 * Class EventController
 * @package controller
 */
class EventController
{

    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var View
     */
    private $view;

    /**
     * EventController constructor.
     * @param EventRepository $eventRepository
     * @param View $view
     */
    public function __construct(EventRepository $eventRepository, View $view)
    {
        $this->eventRepository = $eventRepository;
        $this->view = $view;
    }

    /**
     * Call function findByEventId and display results in view.
     * @param null $id
     */
    public function handleGetByEventId($id = null)
    {
        $event = $this -> eventRepository -> findByEventId($id);
        $this -> view -> show(['events' => $event]);
    }

    /**
     * Call function findByPersonId and display results in view.
     * @param null $id
     */
    public function handleGetByPersonId($id = null)
    {
        $event = $this -> eventRepository -> findByPersonId($id);
        $this -> view -> show(['events' => $event]);
    }

    /**
     * Call function findByTitle and display results in view.
     * @param null $titel
     */
    public function handleGetByTitle($titel = null)
    {
        $event = $this -> eventRepository -> findByTitle($titel);
        $this -> view -> show(['events' => $event]);
    }

    /**
     * Call function findByDate and display results in view.
     * @param null $date
     */
    public function handleGetByDate($date = null)
    {
        $event = $this -> eventRepository -> findByDate($date);
        $this -> view -> show(['events' => $event]);
    }
}