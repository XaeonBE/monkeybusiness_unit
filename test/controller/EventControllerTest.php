<?php

/**
 * Created by PhpStorm.
 * User: Thomas Ven
 * Date: 16/05/2017
 * Time: 16:11
 */

use \model\Event;
use \controller\EventController;

/**
 * Class EventControllerTest
 */
class EventControllerTest extends PHPUnit_Framework_TestCase
{
    /**
     * Will create a mockEventRepository and a mockView.
     */
    public function setUp()
    {
        $this->mockEventRepository = $this->getMockBuilder('model\EventRepository')
            ->getMock();
        $this->mockView = $this->getMockBuilder('view\View')
            ->getMock();
    }

    /**
     * Will destroy the mockEventRepository and the mockView
     */
    public function tearDown()
    {
        $this->mockEventRepository = null;
        $this->mockView = null;
    }

    /**
     * Create Event and call findByEventId
     * Create new eventController and call handleGetByEventId
     * Checks if output is the same as event object.
     */
    public function testHandleGetByEventId_eventFound_stringWithIdName()
    {
        $event = new Event(1, 1, 'testEvent', '2017-06-12 00:00:00');
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findByEventId')
            ->will($this->returnValue($event));
        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                $event = $object['events'];
                printf("[{\"id\"=>%d, \"name\"=>%s}]", $event->getId(), $event->getPid(), $event->getTitle(), $event->getDate());
            }));
        $eventController = new EventController($this->mockEventRepository, $this->mockView);
        $eventController->handleGetByEventId($event->getId());
        $this->expectOutputString(sprintf("[{\"id\"=>%d, \"name\"=>%s}]", $event->getId(),$event->getPid()  ,$event->getTitle() ,  $event->getDate()));
    }

    /**
     * Tests if event can be found and returns empty string
     */
    public function testHandleGetByEventId_eventFound_returnStringEmpty()
    {
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findByEventId')
            ->will($this->returnValue(null));

        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                echo '';
            }));
        $eventController = new EventController($this->mockEventRepository, $this->mockView);
        $eventController->handleGetByEventId(1);
        $this->expectOutputString('');
    }

    /**
     * Create Event and call findByPersonId
     * Create new eventController and call handleGetByPersonId
     * Checks if output is the same as event object.
     */
    public function testHandleGetByPersonId_eventFound_stringWithIdName()
    {
        $event = new Event(1, 1, 'testEvent', '2017-06-12 00:00:00');
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findByPersonId')
            ->will($this->returnValue($event));
        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                $event = $object['events'];
                printf("[{\"id\"=>%d, \"name\"=>%s}]", $event->getId(), $event->getPid(), $event->getTitle(), $event->getDate());
            }));
        $eventController = new EventController($this->mockEventRepository, $this->mockView);
        $eventController->handleGetByPersonId($event->getId());
        $this->expectOutputString(sprintf("[{\"id\"=>%d, \"name\"=>%s}]", $event->getId(),$event->getPid()  ,$event->getTitle() ,  $event->getDate()));
    }

    /**
     * Tests if event can be found and returns empty string
     */
    public function testHandleGetByPersonId_eventFound_returnStringEmpty()
    {
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findByPersonId')
            ->will($this->returnValue(null));

        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                echo '';
            }));
        $eventController = new EventController($this->mockEventRepository, $this->mockView);
        $eventController->handleGetByPersonId(1);
        $this->expectOutputString('');
    }

    /**
     * Create Event and call findByTitle
     * Create new eventController and call handleGetByTitle
     * Checks if output is the same as event object.
     */
    public function testHandleGetByTitel_eventFound_stringWithIdName()
    {
        $event = new Event(1, 1, 'testEvent', '2017-06-12 00:00:00');
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findByTitle')
            ->will($this->returnValue($event));
        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                $event = $object['events'];
                printf("[{\"id\"=>%d, \"name\"=>%s}]", $event->getId(), $event->getPid(), $event->getTitle(), $event->getDate());
            }));
        $eventController = new EventController($this->mockEventRepository, $this->mockView);
        $eventController->handleGetByTitle($event->getTitle());
        $this->expectOutputString(sprintf("[{\"id\"=>%d, \"name\"=>%s}]", $event->getId(),$event->getPid()  ,$event->getTitle() ,  $event->getDate()));
    }

    /**
     * Tests if event can be found and returns empty string
     */
    public function testHandleGetByTitel_eventFound_returnStringEmpty()
    {
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findByTitle')
            ->will($this->returnValue(null));

        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                echo '';
            }));
        $eventController = new EventController($this->mockEventRepository, $this->mockView);
        $eventController->handleGetByTitle('testEvent');
        $this->expectOutputString('');
    }

    /**
     * Create Event and call findByDate
     * Create new eventController and call handleGetByDate
     * Checks if output is the same as event object.
     */
    public function testHandleGetByDate_eventFound_stringWithIdName()
    {
        $event = new Event(1, 1, 'testEvent', '2017-06-12 00:00:00');
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findByDate')
            ->will($this->returnValue($event));
        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                $event = $object['events'];
                printf("[{\"id\"=>%d, \"name\"=>%s}]", $event->getId(), $event->getPid(), $event->getTitle(), $event->getDate());
            }));
        $eventController = new EventController($this->mockEventRepository, $this->mockView);
        $eventController->handleGetByDate($event->getDate());
        $this->expectOutputString(sprintf("[{\"id\"=>%d, \"name\"=>%s}]", $event->getId(),$event->getPid()  ,$event->getTitle() ,  $event->getDate()));
    }

    /**
     * Tests if event can be found and returns empty string
     */
    public function testHandleGetByDate_eventFound_returnStringEmpty()
    {
        $this->mockEventRepository->expects($this->atLeastOnce())
            ->method('findByDate')
            ->will($this->returnValue(null));

        $this->mockView->expects($this->atLeastOnce())
            ->method('show')
            ->will($this->returnCallback(function ($object) {
                echo '';
            }));
        $eventController = new EventController($this->mockEventRepository, $this->mockView);
        $eventController->handleGetByDate('2017-06-12 00:00:00');
        $this->expectOutputString('');
    }
}