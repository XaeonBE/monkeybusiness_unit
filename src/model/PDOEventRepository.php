<?php
/**
 * Created by PhpStorm.
 * User: Thomas Ven
 * Date: 14/05/2017
 * Time: 16:22
 */

namespace model;

/**
 * Class PDOEventRepository
 * @package model
 */
class PDOEventRepository implements EventRepository
{
    /**
     * @var null|\PDO
     */
    private $connection = null;

    /**
     * PDOEventRepository constructor.
     * @param \PDO $connection
     */
    public function __construct(\PDO $connection)
  {
    $this->connection = $connection;
  }

    /**
     * Tries fetching events based on SQL statement and returns it into array, if this fails will throw exception.
     * @param $id
     * @return Event|null
     */
    public function findByEventId($id)
    {

        try {
            $statement = $this->connection->prepare('SELECT * FROM events WHERE id=?');
            $statement->bindParam(1, $id, \PDO::PARAM_INT);
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
            if (count($results) > 0) {
                return new Event($results[0]['id'], $results[0]['persoon_id'], $results[0]['titel'], $results[0]['datum']);
            } else {

                  return null;
            }
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * Tries fetching events based on SQL statement and returns it into array, if this fails will throw exception.
     * @param $id
     * @return Event|null
     */
    public function findByPersonId($id)
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM events WHERE persoon_id=?');
            $statement->bindParam(1, $id, \PDO::PARAM_INT);
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) > 0) {
                return new Event($results[0]['id'], $results[0]['persoon_id'], $results[0]['titel'], $results[0]['datum']);
            } else {
                return null;
            }
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * Tries fetching events based on SQL statement and returns it into array, if this fails will throw exception.
     * @param $title
     * @return Event|null
     */
    public function findByTitle($titel)
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM events WHERE titel =?');
            $statement->bindParam(1, $id, \PDO::PARAM_INT);
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) > 0) {
                return new Event($results[0]['id'], $results[0]['persoon_id'], $results[0]['titel'], $results[0]['datum']);
            } else {
                return null;
            }
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * Tries fetching events based on SQL statement and returns it into array, if this fails will throw exception.
     * @param $datum
     * @return Event|null
     */
    public function findByDate($datum)
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM events WHERE datum =?');
            $statement->bindParam(1, $id, \PDO::PARAM_INT);
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) > 0) {
                return new Event($results[0]['id'], $results[0]['persoon_id'], $results[0]['titel'], $results[0]['datum']);
            } else {
                return null;
            }
        } catch (\Exception $exception) {
            return null;
        }
    }
}
