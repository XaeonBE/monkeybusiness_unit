<?php
/**
 * Created by PhpStorm.
 * User: Thomas Ven
 * Date: 11/05/2017
 * Time: 10:49
 */

namespace view;


/**
 * Interface View
 * @package view
 */
interface View
{
    /**
     * @param array $data
     * @return mixed
     */
    public function show(array $data);
}